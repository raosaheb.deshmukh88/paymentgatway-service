package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentGetWayController {

	@RequestMapping("show")
	public String getPaymentDetails()
	{
		return "HDFC Bank Details";
	}
}
